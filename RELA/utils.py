#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2012-2018 GEM Foundation

Modified: H Crowley

"""
import os
import pandas as pd


def get_filenames(folder="../", start=None, end=None, contain=None):
    '''
    Cretaes a DataFrame with columns ['Path', 'File_Name'] 
    for all the files under the root directory.

    Parameters
    ----------
    :folder :    Root directory with multiple files inside.

    :start :     String. Select the files that start with `start`.

    :end :       String. Select the files that end with `end`.

    :contain :   String or List. Select the files that contain any
                 of the strings.

    Returns
    -------
    DataFrame with columns=['Path', 'File_Name'] with files matching criteria
    '''
    
    data = []
    for root, dirs, files in os.walk(folder, topdown=True):
        if root.find('.git') != -1:
            # Ignore .git folder
            continue
        if len(files) > 0:
            data.extend(zip([root]*len(files), files))
    
    df = pd.DataFrame(data, columns=['Path', 'File_Name'])
    
    df2 = pd.DataFrame()    
    if start:
        df = df[df.File_Name.str.startswith(start)]
    if end:
        df = df[df.File_Name.str.endswith(end)]
    if isinstance(contain, str):
        df = df[df.File_Name.str.contains(contain)]
    elif isinstance(contain, list):        
        for value in contain:
            df2 = df2.append(df[df.File_Name.str.contains(value)],
                             ignore_index=True)
        df2.drop_duplicates(inplace=True)

    if not df2.empty:
        df = df2.copy()

    # assert len(df) != 0, '''\nNo files selected!
    # folder: '{}'
    # start: '{}', 
    # contain: '{}' 
    # end: '{}' '''.format(folder, start, contain, end)
    # Clean data
    df.sort_values(by=['Path', 'File_Name'], inplace=True)
    df.reset_index(drop=True, inplace=True)
    
    return df


def get_tags(columns, tags):
    '''
    List the existing tags in a given exposure file

    :columns: List of columns in exposure model

    :tags:    List of tags present in the exposure model
    
    :returns: Updated list of tags
    '''    
    for col in columns:
        if 'id_' in col and col not in tags:
            tags.append(col)
        if 'name_' in col and col not in tags:
            tags.append(col)
    
    return tags