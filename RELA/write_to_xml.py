#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2012-2018 GEM Foundation

Modified: H Crowley

"""
import xml.etree.ElementTree as ET
import lxml.etree as etree


def exposure_to_xml(csv_files, tags, save_xml):
    '''
    Creates exposure model in xml for multiple csv files
    
    :param csv_files: List of file names to include in exposure
    :param tags: List of tags to include in exposure
    '''
    
    root = ET.Element('nrml')
    root.set('xmlns:gml', "http://www.opengis.net/gml")
    root.set('xmlns', "http://openquake.org/xmlns/nrml/0.4")
    
    exposureModel = ET.SubElement(root, 'exposureModel')
    exposureModel.set('id','exposure')
    exposureModel.set('category','buildings')
    exposureModel.set('taxonomySource','GEM taxonomy')
    
    description = ET.SubElement(exposureModel, 'description')
    description.text = 'exposure model'
    
    conversions = ET.SubElement(exposureModel, 'conversions')
    costTypes = ET.SubElement(conversions, 'costTypes')
    costType = ET.SubElement(costTypes, 'costType')
    costType.set('name','structural')
    costType.set('type','aggregated')
    costType.set('unit','EUR')
    
    occupancyPeriods = ET.SubElement(exposureModel, 'occupancyPeriods')
    occupancyPeriods.text = 'day night transit'
    
    tagNames = ET.SubElement(exposureModel, 'tagNames')
    tagNames.text = ' '.join(tags)
        
    assets = ET.SubElement(exposureModel, 'assets')
    assets.text = ' '.join(csv_files)
    
    # to save
    tree = ET.ElementTree(root)
    tree.write(save_xml)
    x = etree.parse(save_xml)    
    #print (etree.tostring(x, pretty_print = True))
        
    output_file = open(save_xml, 'wb')
    output_file.write(b'<?xml version="1.0" encoding="UTF-8"?>\n')
    output_file.write(etree.tostring(x, pretty_print = True))
    output_file.close()
