#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 16:48:34 2022

@author: helencrowley

This script launches a loss/damage calculation for Icelandic events

"""


#%%
import requests
import os
import run_risk_calculation as run_job
import prepare_exposure as prep_exp
import geopandas as gpd
import shutil

#%%
        
def main ():
                    
    if not os.path.exists('../results/inputs'):
        os.mkdir('../results/inputs')
    if not os.path.exists('../results/outputs'):
        os.mkdir('../results/outputs') 
    if not os.path.exists('../results/inputs/{}'.format(unid)):
        os.mkdir('../results/inputs/{}'.format(unid))
    if not os.path.exists('../results/outputs/{}'.format(unid)):
        os.mkdir('../results/outputs/{}'.format(unid))
        
    ## Option 1: if the website is available to get the shakemaps, lines 36-42 can be used.
    
    # print ('Download European ShakeMap for event id:',(unid)) 
    # grid = requests.get('http://shakemapeu.ingv.it/data/{}/current/products/grid.xml'.format(unid)).text
    # uncertainty = requests.get('http://shakemapeu.ingv.it/data/{}/current/products/uncertainty.xml'.format(unid)).text
    # with open(os.path.join('../results/inputs',unid,'grid_{}.xml'.format(unid)), "w") as f:
        # f.write(grid) 
    # with open(os.path.join('../results/inputs',unid,'uncertainty_{}.xml').format(unid), "w") as f:
        # f.write(uncertainty)
        
     # Option 2: The shakemaps can be downloaded into folder 'data/ShakeMaps-IMO' and directly be used.
     
    shutil.copyfile('../data/ShakeMaps-IMO/'+unid+'/grid.xml', os.path.join('../results/inputs',unid,'grid_{}.xml'.format(unid)))
    shutil.copyfile('../data/ShakeMaps-IMO/'+unid+'/uncertainty.xml', os.path.join('../results/inputs',unid,'uncertainty_{}.xml'.format(unid)))
    
    # download and prepare exposure data for Iceland 

    countries_with_exposure = prep_exp.prep_exposure(unid)       

    #run risk calculations (unless exposure is empty)     
        
    if len(countries_with_exposure) != 0:
        
        run_job.run_loss(unid, countries_with_exposure)
        with open('../results/inputs/log.txt', 'a') as f:
            f.write(f'-----------------{unid}------------------\n Analyses completed, check the log files for any OQ errors\n')
            f.flush()                   
    else:
        
        print('This event does not impact any buildings')
        with open('../results/outputs/{}/log_out.txt'.format(unid), 'a') as f:
            f.write(f'-----------------{unid}------------------\n This event does not impact any buildings\n')
            f.flush()         
         
    os.system('oq reset -y') # remove the oq database 
    os.system('oq engine --duc')
   

#%% Run main function if called as script

if __name__ == "__main__":
     
    # unid = input('Unique ID of the event from the European ShakeMap system: ') 

    # download and parse the ShakeMap system to get details of the event
    url = 'http://shakemapeu.ingv.it/events.json'
    html_content = requests.get(url).text
    eventdata = html_content.split("},")

    # exp_shape = gpd.read_file('../shapefile/Europe.shp') 
   
    unique_ids = [dI for dI in os.listdir('../data/ShakeMaps-IMO/') if os.path.isdir(os.path.join('../data/ShakeMaps-IMO/',dI))]   

    for unid in unique_ids:
        print(unid)
        if os.path.exists('results/inputs/{}'.format(unid)):
            print('{}: Impacts for this event have already been computed previously'.format(unid))
        else:
            main()

